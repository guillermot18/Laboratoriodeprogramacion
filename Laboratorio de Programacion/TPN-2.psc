Proceso Registro_Empleados
	
	Definir Registro Como Entero;
	
	Definir iLong, jLong, i, j, opcion, f, legajo, pass, passok, horaing, horaegre, calculohrs Como Entero;
	
	Dimension Registro[3,5];
	
	iLong<-3;
	
	jLong<-5;
	
	i<-0;
	
	j<-0;
	
	opcion<-0;
	
	legajo<-0;
	
	f<-0;
	
	pass<-0;
	
	passok<-0;
	
	horaing<-0;
	
	horaegre<-0;
	
	calculohrs<-0;
	
	///Carga de datos
	///Legaj-Pass-Ing-Egre-Horas
	///[0,0][0,1][0,2][0,3][0,4]
	///[1,0][1,1][1,2][1,3][1,4]
	///[2,0][2,1][2,2][2,3][2,4]
	
	Registro[0,0]<-123; ///Legajo
	Registro[0,1]<-123; ///Pass
	Registro[0,2]<-0;
	Registro[0,3]<-0;
	Registro[0,4]<-0;
	
	Registro[1,0]<-456; ///Legajo
	Registro[1,1]<-456; ///Pass
	Registro[1,2]<-0;
	Registro[1,3]<-0;
	Registro[1,4]<-0;
	
	Registro[2,0]<-789; ///Legajo
	Registro[2,1]<-789; ///Pass
	Registro[2,2]<-0;
	Registro[2,3]<-0;
	Registro[2,4]<-0;
	
	///Fin carga de datos.
	
	Escribir "<<< Bienvenido al Sistema de Registro Empleados >>>";
	
	Escribir "";
	
	
	Repetir
		Escribir "";
		Escribir "-------------------------";
		Escribir "1. Registrar Ingreso";
		Escribir "2. Registrar Egreso";
		Escribir "3. Salir";
		
		Escribir "";
		Escribir "Elija que desea realizar: ";
		Leer opcion;
		
		Segun opcion Hacer
			
			1:
				Escribir "";
				Escribir "< Registrar Ingreso >";
				Escribir "->Ingrese su Legajo: ";
				Leer legajo;
				
				Para i<-0 Hasta 2 Hacer
					Para j<-0 Hasta 0 Hacer
						
						Si Registro[i,j] = legajo Entonces
							
							passok<- Registro[i,j+1];
							
							Escribir "->Por Favor Ingresar Contraseņa: ";
							Leer pass;
							
							Si passok = pass Entonces
								
								Escribir "->Ingresar Hora de Ingreso (formato 24hrs): ";
								Leer horaing;
								Registro[i,j+2]<-horaing;
								Escribir "Hora Ingresada: ",Registro[i,j+2];
								
							SiNo
								
								Escribir "";
								Escribir "-Contraseņa Incorrecta-";
								
							FinSi
						FinSi
						
					FinPara
				FinPara
				
			2:
				Escribir "";
				Escribir "< Registrar Ingreso >";
				Escribir "->Ingrese su Legajo: ";
				Leer legajo;
				
				Para i<-0 Hasta 2 Hacer
					Para j<-0 Hasta 0 Hacer
						
						Si Registro[i,j] = legajo Entonces
							
							passok<- Registro[i,j+1];
							
							Escribir "->Por Favor Ingresar Contraseņa: ";
							Leer pass;
							
							Si passok = pass Entonces
								
								Escribir "->Ingresar Hora de Egreso (formato 24hrs): ";
								Leer horaegre;
								Registro[i,j+3]<-horaegre;
								Escribir "Hora Ingresada: ",Registro[i,j+3];
								
								Escribir "";
								calculohrs<-Registro[i,j+3] - Registro[i,j+2];
								Escribir "--->Total de Horas Trabajadas: ",calculohrs;
								
							SiNo
								
								Escribir "";
								Escribir "-Contraseņa Incorrecta-";
								
							FinSi
						FinSi
						
					FinPara
				FinPara
				
			3:
				Escribir "";
				Escribir "< Comprobacion Final de Registros >";
				Escribir "->Ingrese su Legajo: ";
				Leer legajo;
				
				f<-1;
				
				Para i<-0 Hasta 2 Hacer
					Para j<-0 Hasta 0 Hacer
						
						Si Registro[i,j] = legajo Entonces
							
							Si Registro[i,j+2]=0 Y Registro[i,j+3]=0  Entonces
								
								Escribir "-Debe Completar los Registros de Ingreso y Egreso, antes de Salir-";
								f<-0;
								
							SiNo
							
								Si Registro[i,j+2]=0 Entonces
									
									Escribir "-Debe Completar el Registro de Ingreso antes de Salir-";
									f<-0;
									
								SiNo 
									
									Si Registro[i,j+3]=0 Entonces
										
										Escribir "-Debe Completar el Registro de Egreso antes de Salir-";
										f<-0;
									
									FinSi									
								FinSi								
							FinSi
						FinSi
						
					FinPara
				FinPara
				
				Si f=0 Entonces
					
					Escribir "";
					Escribir "-Complete sus datos por favor-";
					
				Sino
					
					Escribir "";
					Escribir "<<< Gracias por usar Sistema de Registro Empleados >>>";
					
				FinSi
				
			De Otro Modo:
				Escribir "-Opcion No Valida-";
		FinSegun
		
	Hasta Que f = 1; 
	
FinProceso
