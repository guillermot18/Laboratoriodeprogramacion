Proceso calculo_anios
	definir dias, anios, meses, semanas, dias_total como entero;
	
	escribir "Ingrese cantidad de dias: ";
	Leer dias_total;
	
	anios<-trunc(dias_total/365);
	
	dias_total<-dias_total-anios*365;
	
	meses<-trunc(dias_total/30);
	
	dias_total<-dias_total-meses*30;
	
	semanas<-trunc(dias_total/7);
	
	dias_total<-dias_total-semanas*7;
	
	Escribir "A�os: ", anios;
	Escribir "Meses: ", meses;
	Escribir "Semanas: ", semanas;
	Escribir "Dias: ", dias_total;
	
	
FinProceso
