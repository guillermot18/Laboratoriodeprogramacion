Proceso ordenamiento_arreglo
	definir i, vector, f, j, aux Como Entero;
	Dimension vector[10];
	i<-0;
	j<-0;
	f<-0;
	aux<-0;
	
	Escribir "Se realizara la carga del vector con 10 Nros Aleatorios";
	
	Para i<-0 Hasta 9 Hacer
		vector[i]<-Aleatorio(-10,10);
		Escribir i, " -> ", vector[i];
	FinPara
	
	
	Escribir "";
	Escribir "Se realizara el ordenamiento de menor a mayor del vector";
	
	Para i<-0 Hasta 8 Hacer
		
		Para j<-i+1 Hasta 9 Hacer
		
			Si vector[i] > vector[j] Entonces
				aux<-vector[j];
				vector[i]<-vector[j];
				vector[j]<-aux;
					
			FinSi
				
		FinPara
	
    FinPara
		
	Escribir "";
	Para i<-0 Hasta 9 Hacer
		Escribir i, " -> ", vector[i];
	FinPara
	
FinProceso
