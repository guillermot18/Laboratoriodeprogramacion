Proceso contar_aleatorios
	definir nros, i, n, pos, neg, cero Como Entero;
	Dimension nros[99];
	
	i<-0;
	n<-20;
	pos<-0;
	neg<-0;
	cero<-0;
	
	
	Para i<-0 Hasta n-1 Hacer
		nros[i]<-Aleatorio(-10,10);
	FinPara
	
	Para i<-0 Hasta n-1 Hacer
		Escribir "Nro: ", nros[i];
	FinPara
	
	Para i<-0 Hasta n-1 Hacer
		
		Si nros[i]=0 Entonces
			cero<-cero+1;
		FinSi
		
		Si nros[i]>0 Entonces
			pos<-pos+1;
		FinSi
		
		Si nros[i]<0 Entonces
			neg<-neg+1;
		FinSi
		
	FinPara
	
	Escribir "";
	Escribir "Cantidad de Nros Positivos: ", pos;
	Escribir "";
	Escribir "Cantidad de Nros Negativos: ",neg;
	Escribir "";
	Escribir "Cantidad de Ceros: ",cero;
	Escribir "";
	
FinProceso
