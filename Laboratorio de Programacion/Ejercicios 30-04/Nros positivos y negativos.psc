Proceso nros_positivos_negativos
	definir n, i, acu, pos, neg como Entero;
	
	n<-0;
	pos<-0;
	neg<-0;
		
	Repetir
		
		Escribir "Por favor ingrese nros, termine en cero: ";
		Leer n;
		
		
		Si n>0 Entonces
			pos<-pos+1;
		FinSi
		
		Si n<0 Entonces
			neg<-neg+1;
		FinSi
		
	Hasta Que n=0;
	
	
	Escribir Sin Saltar "Positivos";
	Para i<-1 Hasta pos Hacer
		Escribir Sin Saltar" * ";
	FinPara
	
	Escribir " ";
	
	Escribir Sin Saltar "Negativos";
	Para i<-1 Hasta neg Hacer
		Escribir Sin Saltar" * ";
	FinPara
	
	Escribir " ";
	
	
FinProceso
