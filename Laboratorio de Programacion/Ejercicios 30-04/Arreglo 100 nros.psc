Proceso arreglo_100nros
	definir i, vector, num, num2, num3 Como Entero;
	dimension vector[100];
	i<-0;
	num<-0;
	num2<-0;
	num3<-0;
	
	Escribir "Se realizara la carga del vector con 100 Nros Aleatorios";
	
	Para i<-0 Hasta 99 Hacer
		vector[i]<-Aleatorio(0,20);
		Escribir i, " -> ", vector[i];
	FinPara
	
	Escribir "";
	Escribir "Ingrese un Nro entre 0 y 20 para saber si se encuentra: ";
	Leer num;
	
	Si num >= 0 Y num <=20 Entonces
		Para i<-0 Hasta 99 Hacer
			Si vector[i]=num Entonces
				Escribir "Nro encontrado en la posicion: ", i;
			FinSi
		FinPara
	FinSi
	
	Escribir "";
	Escribir "Ingrese un Nro para reemplazar: ";
	Leer num2;
	Escribir "Ahora ingrese el Nro que quiere ingresar al vector: ";
	Leer num3;
	
	Si num2 >= 0 Y num2 <=20 Entonces
		Para i<-0 Hasta 99 Hacer
			Si vector[i]=num2 Entonces
				Escribir "Nro encontrado en la posicion: ", i;
				vector[i]<-num3;
			FinSi
		FinPara
	FinSi
	
	Escribir "";
	Para i<-0 Hasta 99 Hacer
		vector[i]<-Aleatorio(0,20);
		Escribir i, " -> ", vector[i];
	FinPara
	
	
FinProceso
