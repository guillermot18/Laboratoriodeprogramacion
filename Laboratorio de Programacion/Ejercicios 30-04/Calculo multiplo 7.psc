Proceso calculo_multiplo_7
	definir n Como Entero;
	
	Repetir
		
		Escribir "Ingrese un Nro: ";
		Leer n;
	
		Si n>0 Entonces
		
			Si n%7=0 Entonces
				Escribir "El nro es multiplo de 7";
			SiNo
				Escribir "El nro NO es multiplo de 7";
			FinSi
		
		SiNo
			Escribir "El nro debe ser positivo";
				
		FinSi
	
     Hasta Que n=0;
FinProceso
