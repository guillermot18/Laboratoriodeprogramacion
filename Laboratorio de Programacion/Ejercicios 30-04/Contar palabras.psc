Proceso contar_palabras
	
	definir i, acu, plarga, pcorta Como Entero;
	definir vector Como Caracter;
	Dimension vector[5];
	
	i<-0;	
	acu<-0;
	plarga<-0;
	pcorta<-0;
	
	Para i<-0 Hasta 4 Hacer
		Escribir "Escriba la palabra: ";
		Leer vector[i];
	FinPara
	
	
	Para i<-0 Hasta 4 Hacer
		Escribir "Palabra ", acu, " es: ",vector[i];
		acu<-acu+1;
	FinPara
	
	Para i<-1 Hasta 4 Hacer
		Si (Longitud(vector[i]) > Longitud(vector[plarga])) Entonces
			plarga<-i;
		FinSi
		
		Si (Longitud(vector[i]) < Longitud(vector[pcorta])) Entonces
			pcorta<-i;
		FinSi
		
	FinPara
	
	Escribir "";
	Escribir "La palabra mas larga es: ", vector[plarga];
	Escribir "";
	Escribir "La palabra mas corta es: ", vector[pcorta];
	Escribir "";
FinProceso
