Proceso calculo_nros_multiplos
	Definir n, i como Entero;
	
	i<-0;
	
	Escribir "Ingrese un Nro: ";
	Leer n;
	
	Para i<-1 Hasta n Hacer
		
		Si i%3=0 O i%7=0 Entonces
			Escribir "Nro Multiplo de 3 o 7 es : ", i;
		FinSi
		
	FinPara
	
FinProceso
