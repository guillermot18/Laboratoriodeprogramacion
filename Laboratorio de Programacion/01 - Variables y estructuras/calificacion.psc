Proceso calificacion
	definir nota Como entero;
	Escribir "Escribir la calificacion (6, 7, 8, 9, 10): ";
	leer nota;
	
	Segun nota Hacer
		10:
			Escribir "Excelente";
		9:
			Escribir "Buena";
		8:
			Escribir "Regular";
		7:
			Escribir "Suficiente";
		De Otro Modo:
			Escribir "No Suficiente";
	FinSegun
	
FinProceso
