Proceso cantidadnotas
	definir i, acu, nota, promedio Como Real;
	definir n Como Entero;
	
	n<-0;
	
	escribir "Ingrese Cantidad de Notas:";
	Leer n;
	
	acu<-0;
	promedio<-n;
	i<-0;
	
	Para i<-1 Hasta n Con Paso 1 Hacer
		Escribir "Ingrese Nota: ", i;
		Leer nota;
		acu<-acu+nota;
	FinPara
	
	promedio<-acu/n;
	Escribir "El Promedio de Notas es: ", promedio;
	
FinProceso
