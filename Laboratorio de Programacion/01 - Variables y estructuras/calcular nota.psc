Proceso calculo_nota
	
	definir nota1, nota2, nota3, notafinal, notalab Como Real;
	nota1<-0; 
	nota2<-0; 
	nota3<-0; 
	notafinal<-60; 
	notalab<-0;
	
	escribir "Ingrese el valor de Nota Teorico 1 (entre 0 y 100): ";
	Leer nota1;
	Si nota1>=0 Y nota1<=100 Entonces
		Escribir "Ingrese el valor de Nota Teorico 2 (entre 0 y 100): ";
		leer nota2;
	SiNo
		Escribir "Por favor ingrese un valor entre 0 y 100 para la Nota 1: ";
		leer nota1;
		Escribir "Ingrese el valor de Nota Teorico 2 (entre 0 y 100): ";
		leer nota2;
	FinSi
	
		
	Si nota2>=0 Y nota2<=100 Entonces
		Escribir "Ingrese el valor de Nota Laboratorio (entre 0 y 100): ";
		leer notalab;
	SiNo
		Escribir "Por favor ingrese un valor entre 0 y 100 para Nota 2: ";
		leer nota2;
		Escribir "Ingrese el valor de Nota Laboratorio (entre 0 y 100): ";
		leer notalab;
	FinSi
	
		
	Si notalab>=0 Y notalab<=100 Entonces
		nota3<- (notafinal-notalab*0.3)*(3/0.7)-nota1-nota2;
		Si nota3<100 Entonces
			Escribir "Necesitas ", nota3, " como Nota Teorico 3 para poder aprobar";
		SiNo
			Escribir "Llegas a mas de 100 pudiste aprobar";
		FinSi
	SiNo
		Escribir "Por favor ingrese un valor entre 0 y 100 para la Nota de Laboratorio: ";
		leer notalab;
		Si notalab>=0 Y notalab<=100 Entonces
			nota3<- (notafinal-notalab*0.3)*(3/0.7)-nota1-nota2;
			Si nota3<100 Entonces
				Escribir "Necesitas ", nota3, " para poder aprobar";
			SiNo
				Escribir "Llegas a mas de 60 pudiste aprobar";
			FinSi	
		FinSi
	FinSi
		
FinProceso
