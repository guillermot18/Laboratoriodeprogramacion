Funcion es_bisiesto <- bisiesto (anio)
	
	definir es_bisiesto, divisible4, divisible100, divisible400 Como Logico;
		
	divisible4<-anio mod 4 =0;
	divisible100<-anio mod 100 =0;
	divisible400<-anio mod 400 =0;
	
	es_bisiesto <- (divisible4 Y !divisible100) o divisible400;
FinFuncion

Proceso dias_anio
	
	definir mes, anio Como Entero;
	Escribir "Introduzca mes para saber la cantidad de dias: ";
	leer mes;
	
	Segun mes Hacer
		1, 3, 5, 7, 8, 10, 12:
			Escribir "31 dias";
		2:
			Escribir "Ingrese a�o: ";
			leer anio;
			
			Si bisiesto(anio) Entonces
				Escribir "29 dias";
			SiNo
				Escribir "28 dias";
			FinSi
		4, 6, 9, 11:
			Escribir "30 dias";
		De Otro Modo:
			Escribir "Mes no valido";
	FinSegun
	
FinProceso
